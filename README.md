# Libreria Nacional

_Automatización de la búsqueda de un tema en Página de la Libreria Nacional para tal fin el usuario se debe logear y en al barra de búsqueda digitar la palabra que desee para posteriormente ver desplegada una lista de libros relacionados con la búsqueda para despues Cerrar Sesión._

## Herramientas utilizadas
Arquitectura Screenplay
Gradle
Serenity BDD 
JUnit
Cucumber

## Ejecución del test 
Para ejecutar la prueba se puede abrir el IDE de preferencia, ya sea Eclipse o Intellij, se recomienda Intellij ya que fué el utilizado para escribir el código necesario, luego se procede a clonar el proyecto en alguna ruta local y desde CMD se puede ejecutar el comando:

gradlew.bat clean test aggregate -i

## Autor 

Alejandra Blandón

---
