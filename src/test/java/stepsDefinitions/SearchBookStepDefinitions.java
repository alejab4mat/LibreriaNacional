package stepsDefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import questions.TheMessage;
import questions.TheResult;
import tasks.Go;
import tasks.Login;
import tasks.Look;
import userInterface.PageLibreriaNacionalUserInterface;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class SearchBookStepDefinitions {

    @Managed(driver="chrome")
    private WebDriver navegadorChrome;

    @Before
    public void configuracionInicial() {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled("user").can(BrowseTheWeb.with(navegadorChrome));
    }

    @Given("^that the User want do login in the application$")
    public void that_the_User_want_do_login_in_the_application() {
        theActorInTheSpotlight().wasAbleTo(Open.browserOn(new PageLibreriaNacionalUserInterface()));
    }

    @When("^he enters user (.*) and password (.*)$")
    public void he_enters_user_and_password(String user, String password) {
        theActorInTheSpotlight().attemptsTo(Login.withUserAndPassword(user,password));
    }

    @Then("^he must access the homepage$")
    public void he_must_access_the_homepage() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(TheMessage.onHomepage()));
    }

    @Given("^that the User want do search$")
    public void that_the_User_want_do_search() {
        theActorInTheSpotlight().wasAbleTo(Go.toHomepage());
    }

    @When("^he enters the word (.*)$")
    public void he_enters_the_word(String word) {
        theActorInTheSpotlight().attemptsTo(Look.the(word));
    }

    @Then("^he must see a list of books$")
    public void he_must_see_a_list_of_books() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(TheResult.ofSearch()));
    }
}
