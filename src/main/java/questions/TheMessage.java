package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import userInterface.PageLibreriaNacionalUserInterface;

public class TheMessage implements Question<Boolean> {
    public static TheMessage onHomepage() {
        return new TheMessage();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        Boolean resultado= false;
        if ("Bienvenido a tu cuenta".equals(Text.of(PageLibreriaNacionalUserInterface.MESSAGE).viewedBy(actor).asString())){
            resultado = true;
        } else {
            resultado = false;
        }
        return resultado;
    }
}
