package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class TheResult implements Question<Boolean> {

    public static TheResult ofSearch() {
        return new TheResult();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        Boolean consequence;
        String result = actor.recall("Result of search");
        if (result.equals("PROYECTOS DE PASADO")){
            consequence = true;
        } else {
            consequence = false;
        }
        return consequence;
    }
}
