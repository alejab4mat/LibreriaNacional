package userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://librerianacional.com/")
public class PageLibreriaNacionalUserInterface extends PageObject {
    public static final Target MY_ACCOUNT = Target.the("My account button").
            located(By.xpath("/html/body/app-root/app-header/header/div[2]/div/div/div[2]/div/menu-login-header/div/div/div/button"));
    public static final Target LOG_IN = Target.the("Logn in button").
            located(By.xpath("/html/body/app-root/app-header/header/div[2]/div/div/div[2]/div/menu-login-header/div/div/div/div/a[1]"));
    public static final Target EMAIL_AND_PASSWORD = Target.the("Mail and password button").
            located(By.xpath("/html/body/app-root/div/app-login/div/div/div/div[2]/app-ingreso/div/div[2]/div/div[5]/a/div"));
    public static final Target EMAIL = Target.the("Email field").
            located(By.xpath("/html/body/app-root/div/app-login/div/div/div/div[2]/app-iniciar-sesion/div/div[2]/div[2]/div/form/div[1]/input"));
    public static final Target PASSWORD = Target.the("Password field").
            located(By.xpath("/html/body/app-root/div/app-login/div/div/div/div[2]/app-iniciar-sesion/div/div[2]/div[2]/div/form/div[3]/input"));
    public static final Target LOGN_IN = Target.the("Logn in button").
            located(By.xpath("/html/body/app-root/div/app-login/div/div/div/div[2]/app-iniciar-sesion/div/div[2]/div[2]/div/form/div[5]/button"));
    public static final Target MESSAGE = Target.the("Message tag").
            located(By.xpath("/html/body/app-root/div/app-mi-cuenta/div[2]/div/div/div[1]/div/div/div[1]/div[2]/div/div[2]"));
    public static final Target HOMEPAGE = Target.the("Homepage button").
            located(By.xpath("/html/body/app-root/app-header/header/div[3]/div/div/div[1]/a"));
    public static final Target SEARCH = Target.the("Search bar").
            located(By.xpath("/html/body/app-root/app-header/header/div[3]/div/div/div[3]/div/div[1]/form/div[2]/input[2]"));
    public static final Target RESULT = Target.the("Result of search option").
            located(By.xpath("/html/body/app-root/div/app-libros/div[3]/div/div/div[3]/div[2]/div[1]/div/div/div/div[1]/a"));
    public static final Target ACCOUNT = Target.the("Account button").
            located(By.xpath("/html/body/app-root/app-header/header/div[2]/div/div/div[2]/div/menu-login-header/div/div/div/a"));
    public static final Target LOG_OUT = Target.the("Log out button").
            located(By.xpath("/html/body/app-root/div/app-mi-cuenta/div[2]/div/div/div[1]/div/div/div[2]/div/li[7]/a"));

}
