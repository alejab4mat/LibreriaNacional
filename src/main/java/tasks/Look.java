package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.questions.Text;
import org.openqa.selenium.Keys;

import static userInterface.PageLibreriaNacionalUserInterface.*;

public class Look implements Task {
    String word;
    public Look(String word) {
        this.word = word;
    }

    public static Look the(String word) {
        return Tasks.instrumented(Look.class,word);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        String result;
        actor.attemptsTo(Enter.theValue(word).into(SEARCH).thenHit(Keys.ENTER));
        result = Text.of(RESULT).viewedBy(actor).asString();
        actor.remember("Result of search",result);
        actor.attemptsTo(
                Click.on(ACCOUNT),
                Click.on(LOG_OUT));

    }
}
