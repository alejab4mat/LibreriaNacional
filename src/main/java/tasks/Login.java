package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static userInterface.PageLibreriaNacionalUserInterface.*;

public class Login implements Task {
    String user;
    String password;

    public Login(String user, String password) {
        this.user = user;
        this.password = password;
    }
    public static Login withUserAndPassword(String user, String password) {
        return Tasks.instrumented(Login.class,user,password);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(MY_ACCOUNT),
                Click.on(LOG_IN),
                Click.on(EMAIL_AND_PASSWORD),
                Enter.theValue(user).into(EMAIL),
                Enter.theValue(password).into(PASSWORD),
                Click.on(LOGN_IN));
    }
}
